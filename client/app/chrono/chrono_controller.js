angular.module('app').controller('chronocontroller',['$scope','$http','Party', 'Score', 'Photo', 'Map', 'Chrono', function($scope,$http, Party, Score, Photo, Map, Chrono){
   
    Chrono.startGame=function(){
        $scope.chrono = Chrono;
        Chrono.interval = setInterval(function(){
            if (Chrono.temps != 0) {
                Chrono.temps = Chrono.temps-1;
                $("#chrono").text(Chrono.temps);
            }   else{
                Score.nextImg(Score.currentImg.id);      
                Chrono.temps = 10;     
                $("#chrono").text(Chrono.temps);     
            };
        }, 1000);
    };

}]);