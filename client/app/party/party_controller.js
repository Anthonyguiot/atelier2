angular.module('app').controller('partycontroller',['$scope','$http','Party', 'Score', 'Photo', 'Map', 'Chrono', function($scope,$http, Party, Score, Photo, Map, Chrono){
    $scope.series = [];
    $scope.party = Party;
    

    $http.get('../server/api/index.php/recupSerie').then( function(response){
 
		$.each( response.data, function( key, value ) {
		        	
			$.each( value, function( key2, value2 ) {
				$scope.series.push(value2.serie);
				
                 
			});
		});
        
		$scope.back=function(){
			$("#topScore").addClass('hide');
        	$("#initGame").addClass('hide');
        	$("party").addClass('hide');
        	$(".accNav").removeClass('hide');
            location.reload();
		}
        $scope.game=function(){
        	$("#topScore").addClass('hide');
        	$("#initGame").removeClass('hide');
        	$("party").removeClass('hide');
        	$(".accNav").addClass('hide');
            $('#endBox').addClass('hide');
		};
        $scope.topScore=function(){
        	$("#initGame").addClass('hide');
        	$("#topScore").removeClass('hide');
        	$("party").removeClass('hide');
        	$(".accNav").addClass('hide');
            $('#endBox').addClass('hide');

            $http.get('../server/api/index.php/scoreMeilleur').then( function(response){
                $.each( response.data, function( key, value ) {
                    
                    $.each( value, function( key2, value2 ) {
                        $('#tbody').append("<tr><td>"+value2.pseudo+"</td><td>"+value2.score+"</td></tr>")
                    });
                });
            }); 
        };

        $scope.play=function(){
            $("chrono").removeClass('hide');
            $("party").removeClass('hide');

        	var serie = $("#serieSel").val();
        	var niveau = $("#niveauSel").val();
        	
        	Party.pseudo = $scope.pseudo;
        	$http.get('../server/api/index.php/nouvellePartie').then( function(response){
        		Party.token = response.data.nouvelle_partie.token;
				$("party").addClass("hide");
				console.log(niveau);
				Party.niveau = niveau;
				Score.startGame();
				Photo.startGame(serie);
				Map.startGame($scope.seriesSel.lat, $scope.seriesSel.lon);
				Chrono.startGame();
			}); 
        }
    
	});
    
}]);