angular.module('app').controller('scorecontroller',['$scope','$http','Score', 'Map', 'Party', 'Chrono', function($scope,$http, Score, Map, Party, Chrono){
	
	Score.startGame = function(){
		switch (Party.niveau){
			case 1: 
				Score.dif = 150;
				break;
			case 2:
				Score.dif = 100;
				break;
			case 3:
				Score.dif = 50;
				break;
		}

		$scope.score = Score;

	    $scope.score.point = Score.point;
	    setTimeout(function(){
	    	$("#0").removeClass("ng-hide");
	    	$("map").removeClass("ng-hide");
	    	$("score").removeClass("ng-hide");
	    	$("chrono").removeClass("ng-hide");
	    }, 500);
	    $scope.$watch('score.photos', function(newValue, oldValue){
			Score.currentImg = {img : $scope.score.photos["0"], id : 0};
			

		});
		over = function(){
			$http.get('../server/api/index.php/scoreMeilleur').then( function(response){
                $.each( response.data, function( key, value ) {
                    
                    $.each( value, function( key2, value2 ) {
                        $('#tbody').append("<tr><td>"+value2.pseudo+"</td><td>"+value2.score+"</td></tr>")
                    });
                });
            });
			$("#topScore").removeClass('hide');
        	$("#initGame").addClass('hide');
        	$("party").removeClass('hide');
        	$(".accNav").addClass('hide');
        	$("score").addClass('hide');
        	$("chrono").addClass('hide');
        	$("#endBox").removeClass('hide');
        	$("map").addClass('hide');

		}
	    endGame = function(){
	    	clearInterval(Chrono.interval);
	    	 $http.post("../server/api/index.php/etatPartie/"+Score.point+"/"+Party.pseudo+"/termine?token="+Party.token).then(function(){
                $("#endScore").text(Score.point);

                $http.get("../server/api/index.php/scoreJoueur?token="+Party.token).then( function(response){
	                $.each( response.data, function( key, value ) {
	                    
	                    $.each( value, function( key2, value2 ) {
	                        $("#classement").text((value2.placement+1));
	                    });
	                });
	                over();

            	});

                
            }, function(error){
                console.log(error);
            }); 
	    }
		Score.nextImg = function(id){
			if (Score.currentImg.id == Score.photos.length) {
				endGame();
			}	else{
				Score.currentImg = {img : $scope.score.photos[id++], id : id++};
				Chrono.temps=10;
			};
			$(".img-game").addClass("ng-hide");
			$("#"+$scope.score.currentImg.id).removeClass("ng-hide");
		}
	    
	    Score.calcPoint=function(distance){

	    	var coef = 1;	
			if (Chrono.temps >= 8){
				coef = 4;
			};
			if (Chrono.temps >= 5 && Chrono.temps < 8){
				coef = 2;
			};
			

			if (distance <= Score.dif*3 && distance > Score.dif*2) {
				$scope.score.point = $scope.score.point + (1*coef);
			};
			if (distance <= Score.dif*2 && distance > Score.dif) {
				$scope.score.point = $scope.score.point + (3*coef);
			};
			if (distance <= Score.dif) {
				$scope.score.point = $scope.score.point + (5*coef);
			};
			$("#point").text($scope.score.point);


			Score.nextImg($scope.score.currentImg.id);

		}; 
	}
}]);
