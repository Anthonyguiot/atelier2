angular.module('app').service('Photo',[function(){
	
	var Photo = function(data){
		this.description = data.description;
		this.id          = data.id;
		this.id_serie    = data.id_serie;
		this.lat         = data.lat;
		this.lng         = data.lon;
		this.url         = data.url;
	}
	
	return Photo;
}]);