angular.module('app').controller("mapcontroller", [ '$scope', '$http', 'Map', 'Score', function($scope, $http, Map, Score) {
	Map.startGame = function(latCenter, lngCenter){
		$scope.viewMap = Map;
		$("map").removeClass('hide');
		$scope.$watch('viewMap.view', function(newValue, oldValue){
			if (newValue===true) {
				$scope.map = L.map('map').setView([latCenter, lngCenter], 13);
				L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWltdWlkIiwiYSI6ImNpazQ5cjI0ejAwMjZ2cmtzaTJ1aHRhNXAifQ.NkzUGGMZo4NDS9Yzp7yVtw', {
					maxZoom: 18,
					
					id: 'mapbox.streets'
				}).addTo($scope.map);
				
				$scope.map.on('click', function(e){
					var latlngToClick = L.latLng(e.latlng.lat ,e.latlng.lng);
					marker = L.marker([e.latlng.lat ,e.latlng.lng]).addTo($scope.map);
					var lat = Score.currentImg.img.lat;
					var lng = Score.currentImg.img.lng;
					var latlngImg = L.latLng(+lat, +lng);
					var distance = latlngToClick.distanceTo(latlngImg);
					Score.calcPoint(Math.round(distance));
					
				});
			};
		});
	};

}]);