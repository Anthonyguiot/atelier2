<?php
namespace photolocate\common\model;

use Illuminate\Database\Eloquent\Model as model;

class Serie extends model{
	public $timestamps = false;
	protected $table = 'serie';
	protected $primaryKey = 'id';

	public function __construct() {
	    // rien à faire
	}
}