<?php
namespace photolocate\common\model;

use Illuminate\Database\Eloquent\Model as model;

class Utilisateur extends model{
	public $timestamps = false;
	protected $table = 'utilisateur';
	protected $primaryKey = 'id';
}