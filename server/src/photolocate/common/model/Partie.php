<?php
namespace photolocate\common\model;

use Illuminate\Database\Eloquent\Model as model;

class Partie extends model{
	protected $table = 'partie';
	protected $primaryKey = 'token';
}