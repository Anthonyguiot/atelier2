<?php
namespace photolocate\common\model;

use Illuminate\Database\Eloquent\Model as model;

class Photo extends model{
	public $timestamps = false;
	protected $table = 'photo';
	protected $primaryKey = 'id';
}