<?php
namespace photolocate\backend\controller ;

class AdminController
{

	public function identification($id,$password){
		$app = \Slim\Slim::getInstance();
		$verif = \photolocate\common\model\Utilisateur::select('username','mdp')->where('username','=',$id)->get();
		if (count($verif)==0) {
			$error ="Login inexistant";
			return $error;
		}
		else{
		if (!empty($verif[0])) {
			$hash= $verif[0]->mdp;
			if (password_verify($password, $hash)) {
				$_SESSION['user']=$id;
				$app->redirect($app->urlFor('accueil'));
			}
			else {
				$error ="Mot de passe incorrecte!";
				return $error;
			}
		}

		}
	}


	public function inscription($nom,$prenom,$email,$password,$passwordconfirm,$username){
		$app = \Slim\Slim::getInstance();
		$verif = \photolocate\common\model\Utilisateur::select('username','mdp')->where('username','=',$username)->get();
		if (count($verif)==0) {
			if ($password==$passwordconfirm&&empty(!$password)) {
				$passwordtest= password_hash($password, PASSWORD_DEFAULT);
				$signup= new \photolocate\common\model\Utilisateur;
				$signup->nom =$nom;
				$signup->prenom = $prenom;
				$signup->mail =$email;
				$signup->mdp =$passwordtest;
				$signup->username = $username;
				$signup->save();
				$error="L'inscription s'est effectué avec succés!";
			}
			else{
				$error="Les mots de passe ne sont pas identiques!";
			}
		}
		else{
			$error="Le nom d'utilisateur est deja prit!";
		}
		return $error;
	}


}