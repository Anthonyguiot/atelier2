<?php
namespace photolocate\backend\controller ;

class SerieController
{
	public function createserie($nom){
		$recupererserie = new \photolocate\common\model\Serie;
		$listeserie=$recupererserie->where('nom','=',$nom)->get(); ;
		
		if (count($listeserie)==0) {
				$nom=str_replace(' ','_' , $nom);
				$latlng= $_COOKIE["latlngserie"];
				$latlng=explode('(', $latlng, 2);
				$latlng= $latlng[1];
				$latlng=explode(',', $latlng);
				$lat=$latlng[0];
				$lon=$latlng[1];
				$lon=str_replace(')', '', $lon);
				$create= $signup= new \photolocate\common\model\Serie;
				$create->nom = $nom;
				$create->lat = $lat;
				$create->lon = $lon;
				$create->save();
				$error="La série ".$nom." à été créée!";
				return $error;
		}
		else{
			if ($listeserie[0]->nom == $nom) {
				$error="La série existe déjà!";
				return $error;
			}	
		}

	}

	public function recupererserie(){
		$recupererserie= new \photolocate\common\model\Serie;
		$listeserie=$recupererserie->get();
		foreach ($listeserie as $key => $value) {
			$liste[]= $value->nom." ".$value->lat.$value->lon;
		}
		return $liste;

	}


}