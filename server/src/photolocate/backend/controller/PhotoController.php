<?php
namespace photolocate\backend\controller ;

class PhotoController
{

	public function addphoto($file,$nomville,$description){
	$recupererserie= new \photolocate\common\model\Serie;
	$nomville=explode(' ', $nomville, 2);

	$dossier = $nomville[0];
		if(is_dir("../photos/".$dossier)){
   			
		} else{

  			mkdir("../photos/".$dossier);
		}
		$listeserie=$recupererserie::where('Nom','=',$nomville[0])->get();
		$nomOrigine = $file['photo']['name'];
		$elementsChemin = pathinfo($nomOrigine);
		$extensionFichier = $elementsChemin['extension'];
		$extensionsAutorisees = array("jpeg", "jpg", "png");
			if (!(in_array($extensionFichier, $extensionsAutorisees))) {
			    $error= "Le fichier n'a pas l'extension attendue!";
			} 
			else {    
			    $repertoireDestination ="../../server/photos/".$nomville[0]."/";
			    $nomstr = str_replace(" ", "_", $description);
			    $nomDestination = $nomstr.".".$extensionFichier;
			    move_uploaded_file($file["photo"]["tmp_name"], $repertoireDestination.$nomDestination) ;
			    $error= "Le fichier à bien été ajouté!";
			   
			    
			}

		if (isset($nomDestination)) {
			
			$latlng= $_COOKIE["latlngphoto"];
			$latlng=explode('(', $latlng, 2);
			$latlng= $latlng[1];
			$latlng=explode(',', $latlng,2);
			$lat=$latlng[0];
			$lon=$latlng[1];
			$photo= new \photolocate\common\model\Photo;
			$photo->description= $description;
			$photo->lat=$lat;
			$photo->lon=$lon;
			$photo->status= 'non valide';
			$photo->id_serie=$listeserie[0]->id;
			$photo->url="photos/".$dossier."/".$nomDestination;
			$photo->save();
		}
		return $error;

	}

	public function recupererPhotoNonValide(){
		$app = \Slim\Slim::getInstance();
		$photo = \photolocate\common\model\Photo::select('id','description','lat','lon','url','id_serie')->where('status','=','non valide')->get();
		if (is_object($photo)) {
			$photos = $photo->toArray();

			
			return $photos;
		}
	}

	public function validerPhoto($id,$rootUri){
		$app = \Slim\Slim::getInstance();
		$photo = \photolocate\common\model\Photo::find($id);
		if (is_object($photo)) {
			$photo->status= 'valide';
			$photo->save();

			$app->response->redirect($rootUri.'/verifPhoto');
		}
	}

	public function refuserPhoto($id,$rootUri){
		$app = \Slim\Slim::getInstance();
		$photo = \photolocate\common\model\Photo::find($id);
		if (is_object($photo)) {
			$photo->status= 'refuse';
			$photo->save();

			
			$app->response->redirect($rootUri.'/verifPhoto');
		}
	}


}