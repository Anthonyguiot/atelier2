<?php
namespace photolocate\api\controller ;


class PartieController
{
	public function scoreJoueur($token){
		$app = \Slim\Slim::getInstance();
		$scoreJoueur = \photolocate\common\model\Partie::select('score','pseudo','token')->where('status','=', 'termine')->orderBy('score', 'DESC')->get();
			if (is_object($scoreJoueur)) {
			$s = $scoreJoueur->toArray();
			foreach ($s as $key => $value) { 
				if($value['token'] == $token){
					$tab=[
					'pseudo'=> $value['pseudo'],
					'placement'=> $key,
					'score'=> $value['score']
					];
					$scores[]=$tab;
				}
			}
			$res["scoresJoueur"]=$scores;
			$app->response->headers->set('Content-Type', 'application/json');
			$app->response->setStatus(201);

			return json_encode($res, JSON_FORCE_OBJECT);
		}
		else{
			echo "Mauvais parametre";
			$app->response->setStatus(400);
		}
	}

	/**
   *   Recupere le score des trois meilleur joueur eet celui 
   *   du joueur actuelle
   *
   *   @param $token
   *   @return JSON
   */
	public function scoreMeilleur(){
		$app = \Slim\Slim::getInstance();
		$score = \photolocate\common\model\Partie::select('score','pseudo')->where('status','=', 'termine')->orderBy('score', 'DESC')->get();
		if (is_object($score)) {
			$s = $score->toArray();
			for ($i=0; $i < 3; $i++) {
				$tab=[
				'pseudo'=> $s[$i]['score'],
				'placement'=> $i+1,
				'score'=> $s[$i]['pseudo']
				];
				$scores[]=$tab;
			}
			// $scores = array();
			// $scoresMeilleur = array();
			// foreach ($s as $key => $value) {
			// 	//echo$value['token'];echo'<br/><br/>';
			// 	$tab=[
			// 	'score'=> $value,
			// 	'key'=> $key
			// 	];
			// 	$scores[]=$tab;
			// }
			// foreach ($scores as $key => $value) {
			// 	if($value['score']['token'] == $token){
			// 		$scoresFinaux = array($scores[0]['score']['pseudo']=> $scores[0]['score']['score'],$scores[1]['score']['pseudo']=> $scores[1]['score']['score'],$scores[3]['score']['pseudo']=> $scores[2]['score']['score'],$value['score']['pseudo']=> $value['score']['score']);
			// 	}
			// }
			$res["scores"]=$scores;
			$app->response->headers->set('Content-Type', 'application/json');
			$app->response->setStatus(201);

			return json_encode($res, JSON_FORCE_OBJECT);
		}
		else{
			echo "Mauvais parametre";
			$app->response->setStatus(400);
		}
	}

	/**
   *   Crée une nouvelle partie et génere un nouveau token
   *
   *   @return JSON
   */
	public function nouvellePartie(){
		$app = \Slim\Slim::getInstance();

		$bytes = openssl_random_pseudo_bytes(20);
		$hex   = bin2hex($bytes);

		$nouvellePartie = new \photolocate\common\model\Partie();
		$nouvellePartie->token = $hex;
		$nouvellePartie->status = 'en cour';
		if($nouvellePartie->save()){

			$tab=['token'=> $hex];
			$res["nouvelle_partie"]=$tab;
			$app->response->headers->set('Content-Type', 'application/json');
			$app->response->setStatus(201);
			return json_encode($res, JSON_FORCE_OBJECT);
		}
		else{
			echo "Mauvais parametre";
			$app->response->setStatus(400);
		}
	}

	/**
   *   Change l'état de la partie de en cour à terminé
   *   Enregistre le score du joueur
   *   Enregistre le nombre de photos trouvées
   *   
   *
   *   @param $id_partie
   *   @param $nb_photo
   *   @param $score
   *   @param $etat
   *   @return JSON
   */
	public function etatPartie($token,$pseudo,$score,$etat){
		$app = \Slim\Slim::getInstance();
		$finPartie = \photolocate\common\model\Partie::find($token);
		$finPartie->status = $etat;
		$finPartie->score = $score;
		$finPartie->pseudo = $pseudo;
		if($finPartie->save()){
			$app->response->setStatus(201);
		}
		else{
			echo "Mauvais parametre";
			$app->response->setStatus(400);
		}
	}

}