<?php
namespace photolocate\api\controller ;


class PhotoController
{
	/**
   *   Recupere les photos pour une série donnée et un niveau donnée
   *
   *   @param $id
   *   @return JSON
   */
	public function recupPhotoSerie($id_serie,$niveau){
		$app = \Slim\Slim::getInstance();
		$photo = \photolocate\common\model\Photo::select('id','description','lat','lon','url','id_serie')->where('id_serie','=',$id_serie)->where('status','=','valide')->get();
		if (is_object($photo)) {
			$p = $photo->toArray();
			$photos = array();
			
			$nb = count($p);
			//echo $niveau;

			if($niveau == 1){
				$val_max = 10;
				$nb_a_tirer = 10;
				//echo'niveau1';
			}
			elseif($niveau == 2){
				$val_max = 12;
				$nb_a_tirer = 12;
				//echo'niveau2';
			}
			else{
				$val_max = 14;
				$nb_a_tirer = 14;
				//echo'niveau3';
			}

			
			$val_min = 1;
			$tab_result = array();
			while($nb_a_tirer != 0 )
			{
				$nombre = mt_rand($val_min, $val_max);
				if( !in_array($nombre, $tab_result) )
				{
					$tab_result[] = $nombre;
					$nb_a_tirer--;
				}
			}

			if($val_max == count($p) || $val_max <= count($p)){
				foreach ($tab_result as $key => $value) {
					foreach ($p as $key2 => $value2) {
						if($value2['id'] == $value){
							$tab=[
							'photo'=> $value2
							];
							$photos[]=$tab;
						}
					}
				}
			}
			else{
				foreach ($p as $key2 => $value2) {
					$tab=[
					'photo'=> $value2
					];
					$photos[]=$tab;
				}
			}
			

			$res["photos"]=$photos;
			$app->response->headers->set('Content-Type', 'application/json');
			$app->response->setStatus(201);
			return json_encode($res, JSON_FORCE_OBJECT);
		}
		else{
			echo "Mauvais parametre";
			$app->response->setStatus(400);
		}
	}
}