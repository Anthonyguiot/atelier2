<?php
namespace photolocate\api\controller ;


class SerieController
{
	/**
   *   Recupere toutes les séries
   *
   *   @return JSON
   */
	public function recupSerie(){
		$app = \Slim\Slim::getInstance();
		$serie = \photolocate\common\model\Serie::select('id','nom','lon','lat')->get();
		if (is_object($serie)) {
			$s = $serie->toArray();
			$series = array();
			foreach ($s as $key => $value) {
				$tab=[
				'serie'=> $value
				];
				$series[]=$tab;
			}
			$res["series"]=$series;
			$app->response->headers->set('Content-Type', 'application/json');
			$app->response->setStatus(201);

			return json_encode($res, JSON_FORCE_OBJECT);;
		}
		else{
			echo "Mauvais parametre";
			$app->response->setStatus(400);
		} 
	}
}