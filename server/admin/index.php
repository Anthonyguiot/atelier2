<?php
require_once "../vendor/autoload.php";


\app\App::EloConfigure('../conf/config.ini');
$app = new \Slim\Slim();
//$app = new \Slim\Slim(['view' => new \Slim\Views\Twig(),'templates.path' =>'../src/photolocate/backend/templates']); 
session_start();
//accueil
$app->get('/', function () use ($app) {
	$urllogin=$app->urlFor('admin');
	$urlsubscribe=$app->urlFor('inscription');
	$app = new \Slim\Slim(['view' => new \Slim\Views\Twig(),'templates.path' =>'../src/photolocate/backend/templates']); 
	$app->render('baseaccueil.html.twig',array('urllogin'=> $urllogin,'urlsubscribe'=> $urlsubscribe));
})->name("racine");

//Page de login
$app->get('/admin', function() use ($app) {
	$app = new \Slim\Slim(['view' => new \Slim\Views\Twig(),'templates.path' =>'../src/photolocate/backend/templates']); 
	$app->render( 'admin.html.twig',
	array('id' => 'Identifiant:', 
	'mdp' =>  'Mot de passe:','title'=> 'Authentification:'));
})->name("admin");

//login
$app->post('/login', function () use ($app) {
	$app = new \Slim\Slim(['view' => new \Slim\Views\Twig(),'templates.path' =>'../src/photolocate/backend/templates']); 
	$id   = $app->request->post('id');
	$mdp  = $app->request->post('password');
	$c = new \photolocate\backend\controller\AdminController();
	$error = $c->identification($id,$mdp);
	$app->render( 'admin.html.twig',
	array('id' => 'Identifiant:', 
	'mdp' =>  'Mot de passe:','title'=> 'Authentification:','error' => $error ));
});
//logout
$app->get('/logout', function () use ($app) {
	session_destroy();
	$app->redirect($app->urlFor('racine'));
})->name("logout");
//ajout photo
$app->get('/addphoto', function () use ($app) {
	if (isset($_SESSION['user'])) {
		$urlphoto=$app->urlFor('photo');
		$urllogout=$app->urlFor('logout');
		$urlserie=$app->urlFor('serie');
		$accueil=$app->urlFor('accueil');
		$photoNonValide=$app->urlFor('photoNonValide');
		$serie = new \photolocate\backend\controller\SerieController();
		$serie=$serie->recupererserie();
		$app = new \Slim\Slim(['view' => new \Slim\Views\Twig(),'templates.path' =>'../src/photolocate/backend/templates']); 

		$app->render('addphoto.html.twig',
		array('title'=> 'Ajout de photo:' , 'serie' => $serie,'urlphoto'=> $urlphoto ,'urllogout' => $urllogout,'urlserie'=> $urlserie,'photoNonValide' => $photoNonValide,'accueil'=> $accueil));
	}
	else{
		$app->redirect($app->urlFor('admin'));
	}
})->name("photo");

$app->get('/addserie', function () use ($app) {
	if (isset($_SESSION['user'])) {
		$urlphoto=$app->urlFor('photo');
		$urllogout=$app->urlFor('logout');
		$urlserie=$app->urlFor('serie');
		$accueil=$app->urlFor('accueil');
		$photoNonValide=$app->urlFor('photoNonValide');
		$serie = new \photolocate\backend\controller\SerieController();
		$serie=$serie->recupererserie();
		$app = new \Slim\Slim(['view' => new \Slim\Views\Twig(),'templates.path' =>'../src/photolocate/backend/templates']); 
		$app->render('addserie.html.twig',
		array('title'=> 'Ajout de serie:' ,'serie' => $serie,'urlphoto'=> $urlphoto ,'urllogout' => $urllogout,'urlserie'=> $urlserie,'photoNonValide' => $photoNonValide,'accueil'=> $accueil));
	}
	else{
		$app->redirect($app->urlFor('admin'));
	}
})->name("serie");

$app->post('/create', function () use ($app) {
	$urlphoto=$app->urlFor('photo');
	$urllogout=$app->urlFor('logout');
	$urlserie=$app->urlFor('serie');
	$accueil=$app->urlFor('accueil');
	$photoNonValide=$app->urlFor('photoNonValide');
	$serie = new \photolocate\backend\controller\SerieController();
	$serieliste=$serie->recupererserie();
	$nomserie  = $app->request->post('nom');
	$serie = new \photolocate\backend\controller\SerieController();
	$error=$serie->createserie($nomserie);
	$app = new \Slim\Slim(['view' => new \Slim\Views\Twig(),'templates.path' =>'../src/photolocate/backend/templates']); 
	$app->render('addserie.html.twig',array('title'=> 'Ajout de serie:','serie' => $serieliste,'error' => $error,'urlphoto'=> $urlphoto ,'urllogout' => $urllogout,'urlserie'=> $urlserie,'photoNonValide' => $photoNonValide,'accueil'=> $accueil ));

});



//upload
$app->post('/upload', function () use ($app) {
	$urlphoto=$app->urlFor('photo');
	$urllogout=$app->urlFor('logout');
	$urlserie=$app->urlFor('serie');
	$accueil=$app->urlFor('accueil');
		$photoNonValide=$app->urlFor('photoNonValide');
	$photoNonValide=$app->urlFor('photoNonValide');
	$app = new \Slim\Slim(['view' => new \Slim\Views\Twig(),'templates.path' =>'../src/photolocate/backend/templates']); 
	$photo  = $app->request->post('photo');
	$nomville = $app->request->post('ville');
	$description = $app->request->post('description');
	$serie = new \photolocate\backend\controller\SerieController();
	$serie=$serie->recupererserie();
	$upload = new \photolocate\backend\controller\PhotoController();
	$error=$upload->addphoto($_FILES,$nomville,$description);
	$app->render('addphoto.html.twig',array('title'=> 'Ajout de photo:','error' => $error ,'serie' => $serie,'urlphoto'=> $urlphoto ,'urllogout' => $urllogout,'urlserie'=> $urlserie,'photoNonValide' => $photoNonValide,'accueil'=> $accueil));

})->name("upload");


//verif photo
$app->get('/verifPhoto', function () use ($app) {
	if($_SESSION['user'] == 'root'){
	$urlphoto=$app->urlFor('photo');
	$urllogout=$app->urlFor('logout');
	$urlserie=$app->urlFor('serie');
	$accueil=$app->urlFor('accueil');
	$photoValider=$app->urlFor('photoValider');
	$photoRefuser=$app->urlFor('photoRefuser');

	$app = new \Slim\Slim(['view' => new \Slim\Views\Twig(),'templates.path' =>'../src/photolocate/backend/templates']); 

	$photos = new \photolocate\backend\controller\PhotoController();
	$photos = $photos->recupererPhotoNonValide();

	$req = $app->request;
	$rootUri = $req->getRootUri();

	$app->render('verifphoto.html.twig',array('rootUri'=> $rootUri,'photoValider'=> $photoValider,'photoRefuser'=> $photoRefuser ,'title'=> 'Vérification des photos:','photos'=> $photos ,'urlphoto'=> $urlphoto ,'urllogout' => $urllogout,'urlserie'=> $urlserie,'accueil'=> $accueil));
	}else{
	$urlphoto=$app->urlFor('photo');
	$urllogout=$app->urlFor('logout');
	$urlserie=$app->urlFor('serie');
	$accueil=$app->urlFor('accueil');
	$app = new \Slim\Slim(['view' => new \Slim\Views\Twig(),'templates.path' =>'../src/photolocate/backend/templates']); 

	$photos = new \photolocate\backend\controller\PhotoController();
	$photos = $photos->recupererPhotoNonValide();

	$req = $app->request;
	$rootUri = $req->getRootUri();

	$app->render('verifphoto.html.twig',array('title'=> 'Desole vous n\'avez pas les droit super administrateur','urlphoto'=> $urlphoto ,'urllogout' => $urllogout,'urlserie'=> $urlserie,'accueil'=> $accueil));
	}
	

})->name("photoNonValide");

//valider les photos
$app->get('/validerPhoto/:id', function ($id) use ($app) {
	$req = $app->request;
	$rootUri = $req->getRootUri();
	$photo = new \photolocate\backend\controller\PhotoController();
	$photo->validerPhoto($id,$rootUri);

})->name("photoValider");

//refuser les photos
$app->get('/refuserPhoto/:id', function ($id) use ($app) {
	$req = $app->request;
	$rootUri = $req->getRootUri();
	$photo = new \photolocate\backend\controller\PhotoController();
	$photo->refuserPhoto($id,$rootUri);

})->name("photoRefuser");


$app->get('/inscription', function () use ($app) {
	$urllogin=$app->urlFor('admin');
	$app = new \Slim\Slim(['view' => new \Slim\Views\Twig(),'templates.path' =>'../src/photolocate/backend/templates']); 
	$app->render( 'inscription.html.twig',
	array('title'=> 'Inscription:','urllogin'=> $urllogin ));
})->name("inscription");

//Accueil une fois loguer
$app->get('/accueil', function () use ($app) {
	if (isset($_SESSION['user'])) {
		$urlphoto=$app->urlFor('photo');
		$urllogout=$app->urlFor('logout');
		$urlserie=$app->urlFor('serie');
		$photoNonValide=$app->urlFor('photoNonValide');
		$app = new \Slim\Slim(['view' => new \Slim\Views\Twig(),'templates.path' =>'../src/photolocate/backend/templates']); 

		$app->render( 'accueil.html.twig',
		array('urlphoto'=> $urlphoto ,'urllogout' => $urllogout,'name' => $_SESSION['user'],'urlserie'=> $urlserie,'photoNonValide' => $photoNonValide));
	}
	else{
		$app->redirect($app->urlFor('admin'));
	}
})->name("accueil");

//Inscription

$app->post('/signup', function () use ($app) {
	$urllogin=$app->urlFor('admin');
	$nom   = $app->request->post('nom');
	$prenom   = $app->request->post('prenom');
	$email   = $app->request->post('email');
	$password   = $app->request->post('password');
	$passwordconfirm   = $app->request->post('password-confirmation');
	$username = $app->request->post('username');
	$signup = new \photolocate\backend\controller\AdminController();
	$error=$signup->inscription($nom,$prenom,$email,$password,$passwordconfirm,$username);
	$app = new \Slim\Slim(['view' => new \Slim\Views\Twig(),'templates.path' =>'../src/photolocate/backend/templates']); 
	$app->render( 'inscription.html.twig',array('title'=> 'Inscription:', 'error'=>$error,'urllogin'=> $urllogin ));

});

$app->run();

