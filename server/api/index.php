<?php
require_once "../vendor/autoload.php";

\app\App::EloConfigure('../conf/config.ini');

$app = new \Slim\Slim();

/**
*
*	Verifie que le token existe
*
**/
function token(){
$app = \Slim\Slim::getInstance();
$recup_token = $app->request->get('token');
$token=\photolocate\common\model\Partie::select('token')->where('token','=',$recup_token)->get();
	if (isset($token[0])) {
		$app->response->setStatus(201);
	}
	else{
		$app->response->setStatus(403);
		echo json_encode("Token non valide");
		$app->stop();
	}
}

/**
*
*	Recupere toutes les séries
*
**/
$app->get('/recupSerie' , function() use ($app){
	$c = new \photolocate\api\controller\SerieController();
	$res = $c->recupSerie();
	echo $res;
});

/**
*
*	Recupere les photos pour une série donnée
*
**/
$app->get('/recupPhotoSerie' ,'token', function() use ($app){
	$c = new \photolocate\api\controller\PhotoController();
	$id_serie = $app->request->get('id_serie');
	$niveau = $app->request->get('niveau');
	$res = $c->recupPhotoSerie($id_serie,$niveau);
	echo $res;
});


$app->get('/scoreJoueur' ,'token', function() use ($app){
	$c = new \photolocate\api\controller\PartieController();
	$token = $app->request->get('token');
	$res = $c->scoreJoueur($token);
	echo $res;
});

/**
*
*	Recupere le score des trois meilleur joueur eet celui 
*   du joueur actuelle
*
**/
$app->get('/scoreMeilleur', function() use ($app){
	$c = new \photolocate\api\controller\PartieController();
	$res = $c->scoreMeilleur();
	echo $res;
});

/**
*
*	Crée une nouvelle partie et génere un nouveau token
*
**/
$app->get('/nouvellePartie' , function() use ($app){
	$c = new \photolocate\api\controller\PartieController();
	$res = $c->nouvellePartie();
	echo $res;
});

/**
*
*	Change l'état de la partie de en cour à terminé
*	Enregistre le score du joueur
* 	Enregistre le nombre de photos trouvées
*
**/
$app->post('/etatPartie/:score/:pseudo/:etat' ,'token', function($score,$pseudo,$etat) use ($app){
	$c = new \photolocate\api\controller\PartieController();
	$token = $app->request->get('token');
	$c->etatPartie($token,$pseudo,$score,$etat);
});

$app->run();

